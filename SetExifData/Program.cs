﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using ExifLibrary;
using System.Drawing;
using System.Drawing.Imaging;

using Image = System.Drawing.Image;

namespace SetExifData
{
    class Program
    {
        static int Main(string[] args)
        {
            foreach (var file in args)
            {
                var fi = new FileInfo(file);
                if (fi.Extension != ".jpg")
                {
                    continue;
                }
                var foldername = fi.Directory.FullName.Split('\\').Last();

                ImageFile data = ImageFile.FromFile(fi.FullName);

                SetProperty(data,ExifTag.Copyright, Properties.Settings.Default.AuthorName);
                SetProperty(data,ExifTag.ImageDescription, $"{foldername}, © {Properties.Settings.Default.AuthorName}");
                SetProperty(data,ExifTag.WindowsSubject, $"{foldername}, © {Properties.Settings.Default.AuthorName}");
                SetProperty(data,ExifTag.WindowsTitle, $"{foldername}, © {Properties.Settings.Default.AuthorName}");
                   

                Directory.CreateDirectory(fi.DirectoryName + "\\output");
                data.ToImage().Save(fi.DirectoryName + "\\output\\" + fi.Name, ImageFormat.Jpeg);

            }
            return 0;
        }

        private static void SetProperty(ImageFile data, ExifTag tag, string value)
        {
            var pic2 = data.Properties.FirstOrDefault(p => p.Tag == tag);
            if (pic2 == null)
            {
                data.Properties.Add(tag, value);
            }
            else
            {
                pic2.Value = value;
            }
        }
    }
    public static class ExtensionMethods
    {
        public static Image ToImage(this ImageFile img)
        {
            MemoryStream stream = new MemoryStream();
            img.Save(stream);
            return Image.FromStream(stream);
        }
    }
}
