The music magazine monkeypress.de reports regularly about live concerts, reports about music news and CD reviews. 
For the photographers there is this tool, with which you can easily tag pictures. Based on an idea by Markus Hillgärtner it works as follows:

Preparations:
In the settings file you have to set the name of the photographer.

The photos must be stored in the following structure:
```
- Southside Festival 2020
---- Iron Maiden
------- 008.jpg
------- 009.jpg
------- 010.jpg
---- Elvis Presley
------- 005.jpg
------- 006.jpg
------- 007.jpg
---- Metallica
------- 001.jpg
------- 002.jpg
------- 003.jpg
------- 004.jpg
```
Then you can drag the images to SetExifTool.exe and it exports the images and sets the EXIF data with title and author.
 
 Inline-style: 
![Demo](docs/img/SetExifData.gif "Demo")

# Road-Map
 - Processing of entire folder structures
 - Integration in den Windows Explorer
